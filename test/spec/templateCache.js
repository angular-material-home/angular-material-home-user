angular.module('ngMaterialHomeUser').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/amh-account.html',
    " <div></div>"
  );


  $templateCache.put('views/amh-forgot-password.html',
    ""
  );


  $templateCache.put('views/amh-login.html',
    ""
  );


  $templateCache.put('views/amh-profile.html',
    ""
  );


  $templateCache.put('views/amh-recover-password.html',
    ""
  );


  $templateCache.put('views/amh-user-area.html',
    ""
  );


  $templateCache.put('views/amh-user.html',
    ""
  );


  $templateCache.put('views/settings/amhuser-login.html',
    " <md-list class=wb-setting-panel>  <md-subheader class=md-no-sticky>Before login</md-subheader>  <wb-ui-setting-on-off-switch title=\"Forgot password link?\" icon=vpn_key value=wbModel.content.forgot_password> </wb-ui-setting-on-off-switch>  <wb-ui-setting-on-off-switch title=\"Register button?\" icon=person_add value=wbModel.content.register> </wb-ui-setting-on-off-switch> <md-subheader class=md-no-sticky>After login</md-subheader>  <wb-ui-setting-on-off-switch title=\"Profile link?\" icon=account_circle value=wbModel.content.user_area> </wb-ui-setting-on-off-switch> <wb-ui-setting-on-off-switch title=\"Account link?\" icon=account_circle value=wbModel.content.account> </wb-ui-setting-on-off-switch>  <wb-ui-setting-on-off-switch title=\"Logout button?\" icon=settings_power value=wbModel.content.logout> </wb-ui-setting-on-off-switch> </md-list>"
  );


  $templateCache.put('views/widgets/amhuser-login.html',
    " <div layout=column> <md-progress-linear ng-disabled=\"!(ctrl.loginProcess || ctrl.logoutProcess)\" style=\"margin: 0px; padding: 0px\" md-mode=indeterminate class=md-primary md-color> </md-progress-linear>   <div style=\"text-align: center\" layout-margin ng-show=\"!ctrl.loginProcess && ctrl.loginState === 'fail'\"> <p><span md-colors=\"{color:'warn'}\" translate>{{loginMessage}}</span></p> </div>  <form ng-show=appUser.anonymous name=ctrl.myForm ng-submit=login(credit) layout=column layout-margin> <md-input-container> <label translate>Username</label> <input ng-model=credit.login name=username required> <div ng-messages=ctrl.myForm.username.$error> <div ng-message=required translate>This field is required.</div> </div> </md-input-container> <md-input-container> <label translate>Password</label> <input ng-model=credit.password type=password name=password required> <div ng-messages=ctrl.myForm.password.$error> <div ng-message=required translate>This field is required.</div> </div> </md-input-container>     <div ng-if=\"app.captcha.engine==='recaptcha'\" vc-recaptcha ng-model=credit.g_recaptcha_response theme=\"app.captcha.theme || 'light'\" type=\"app.captcha.type || 'image'\" key=app.captcha.recaptcha.key lang=\"app.captcha.language || 'fa'\"> </div> <input hide type=\"submit\"> </form> <div ng-show=appUser.anonymous layout=column layout-align=none layout-gt-xs=row layout-align-gt-xs=\"center center\" layout-margin> <a ng-show=wbModel.content.forgot_password href=users/reset-password style=\"text-decoration: none\" ui-sref=forget flex-order=1 flex-order-gt-xs=-1>{{'forgot your password?' | translate}}</a> <md-button ng-disabled=ctrl.myForm.$invalid flex-order=-1 flex-order-gt-xs=1 class=\"md-primary md-raised\" ng-click=login(credit)>{{'login' | translate}}</md-button> <md-button ng-show=wbModel.content.register ng-href=users/signup flex-order=0 flex-order-gt-xs=0 class=md-raised> {{ 'sign up' | translate }} </md-button> </div>  <div layout-margin ng-show=!appUser.anonymous layout=column layout-align=\"none center\"> <img style=\"max-width: 150px;border-radius: 50%\" src=\"{{'/api/user/'+appUser.current.id+'/avatar'}}\"> <span>{{appUser.current.first_name}} {{appUser.current.last_name}}</span> <h3>{{appUser.current.login}}</h3> </div>  <div ng-show=!appUser.anonymous layout=column layout-align=none layout-gt-xs=row layout-align-gt-xs=\"center center\" layout-margin> <md-button ng-show=wbModel.content.logout ng-click=logout() flex-order=0 flex-order-gt-xs=0 class=\"md-raised md-accent\">  {{'Logout' | translate}} </md-button> <md-button ng-show=wbModel.content.user_area ng-href=users/profile flex-order=1 flex-order-gt-xs=-1 class=md-raised>  {{'Profile' | translate}} </md-button> <md-button ng-show=wbModel.content.account ng-href=users/account flex-order=1 flex-order-gt-xs=-1 class=md-raised>  {{'Account' | translate}} </md-button> </div> </div>"
  );


  $templateCache.put('views/widgets/amhuser-page-not-found.html',
    "<div layout=row> <md-button ng-click=createContent()> <wb-icon>new</wb-icon> <span translate>create new page</span> </md-button> </div>"
  );

}]);
