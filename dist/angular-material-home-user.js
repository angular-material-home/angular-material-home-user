/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/*
 * 
 */
angular.module('ngMaterialHomeUser', [
	'ngMaterialHome'
]);
/*
 * @Deprecated
 * Paths are moved to mblowfish-core. To use those paths add dependency to mblowfish-core version ^1.1.0
 */


///*
// * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
// * 
// * Permission is hereby granted, free of charge, to any person obtaining a copy
// * of this software and associated documentation files (the "Software"), to deal
// * in the Software without restriction, including without limitation the rights
// * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// * copies of the Software, and to permit persons to whom the Software is
// * furnished to do so, subject to the following conditions:
// * 
// * The above copyright notice and this permission notice shall be included in all
// * copies or substantial portions of the Software.
// * 
// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// * SOFTWARE.
// */
//'use strict';
//
//angular.module('ngMaterialHomeUser')
///*
// * State of the user management
// */
//.config(function($routeProvider) {
//	$routeProvider//
//	/**
//	 * @ngdoc ngRoute
//	 * @name /users/account
//	 * @description Details of the current account
//	 */
//	.when('/users/account', {
//		templateUrl : 'views/amh-account.html',
//		controller : 'MbAccountCtrl',
//		protect: true
//	})
//	/**
//	 * @ngdoc ngRoute
//	 * @name /user/account
//	 * @description Details of the current account
//	 */
//	.when('/user/account', {
//		templateUrl : 'views/amh-account.html',
//		controller : 'MbAccountCtrl',
//		protect: true
//	})
//	/**
//	 * @ngdoc ngRoute
//	 * @name /users/profile
//	 * @description Profile of the current account
//	 */
//	.when('/users/profile', {
//		templateUrl : 'views/amh-profile.html',
//		controller : 'AmhProfileCtrl'
//	})
//	/**
//	 * @ngdoc ngRoute
//	 * @name /user/profile
//	 * @description Profile of the current account
//	 */
//	.when('/user/profile', {
//		templateUrl : 'views/amh-profile.html',
//		controller : 'AmhProfileCtrl'
//	})
//	/**
//	 * @ngdoc ngRoute
//	 * @name /users/user-area
//	 * @description User area of the current account
//	 */
//	.when('/users/user-area', {
//		templateUrl : 'views/amh-user-area.html',
//		controller : 'AmhUserAreaCtrl',
//		protect: true
//	})
//	
//	// Reset forgotten password
//	.when('/users/reset-password', {
//		templateUrl : 'views/amh-forgot-password.html',
//		controller : 'AmhUserPasswordCtrl'
//	})//
//	.when('/users/reset-password/token', {
//		templateUrl : 'views/amh-recover-password.html',
//		controller : 'AmhUserPasswordCtrl'
//	})//
//	.when('/users/reset-password/token/:token', {
//		templateUrl : 'views/amh-recover-password.html',
//		controller : 'AmhUserPasswordCtrl'
//	})//
//
//	// Login
//	.when('/users/login', {
//		templateUrl : 'views/amh-login.html',
//		controller : 'AmhUserAccountCtrl'
//	})
//	
//	/**
//	 * @ngdoc ngRoute
//	 * @name /users/:userId
//	 * @description Detail of the account
//	 */
//	.when('/users/:userId', {
//		templateUrl : 'views/amh-user.html',
//		controller : 'AmhUserCtrl'
//	})
//	
//	;//
//});

/*
 * @Deprecated
 * Note: Hadi, 1397-03-08: Use MbAccountCtrl in mblowfish-core module version ^1.1.0
 */ 

/*
 * @Deprecated
 * Note: Hadi, 1397-03-08: Use MbPasswordCtrl in mblowfish-core module version ^1.1.0
 */
/*
 * @Deprecated
 * Note: Hadi, 1397-03-08: Use MbProfileCtrl in mblowfish-core module version ^1.1.0
 */ 
/*
 * @Deprecated
 * 
 * This controller is deprecated. Instead use MbAccountCtrl, MbProfileCtrl and MbPasswordCtrl 
 * in mblowfish-core module version ^1.1.0
 */
/*
 * @Deprecated
 * 
 * This controller is deprecated. Instead use MbAccountCtrl, MbProfileCtrl or MbPasswordCtrl 
 * in mblowfish-core module version ^1.1.0 appropriately.
 */
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialHomeUser')

/**
 * Load widgets
 */
.run(function($settings) {
	$settings.newPage({
		type: 'amhuser-login-settings',
		label : 'Login settings',
		description : 'Settings for login widget',
//		controller: 'AmWbCommonActionCtrl',
		icon : 'input',
		templateUrl : 'views/settings/amhuser-login.html'
	});
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialHomeUser')
/**
 * دریچه‌های محاوره‌ای
 */
.run(function($widget) {

	// Login widget
	$widget.newWidget({
		type: 'AmhUserLoginWidget',
		title : 'Login',
		label : 'Login',
		description : 'A widget to login/logout users.',
		icon: 'input',
		groups: ['user'],
		templateUrl : 'views/widgets/amhuser-login.html',
		controller: 'MbAccountCtrl',
		setting:['amhuser-login-settings'],
	});
	// Page
	$widget.newWidget({
		type: 'PageNotFoundErrorAction',
		templateUrl : 'views/widgets/amhuser-page-not-found.html',
		title : 'Page not found action',
		label : 'Page not found action',
		description : 'Actions which are needed in page not found error status.',
		image : 'images/wb/content.svg',
		help : 'http://dpq.co.ir/more-information-link',
		setting: [ 'description', 'border',
			'background',
			'selfLayout' ],
			data : {
				type : 'PageNotFoundErrorAction',
				style : {},
			}
	});
});
angular.module('ngMaterialHomeUser').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/amh-account.html',
    " <div></div>"
  );


  $templateCache.put('views/amh-forgot-password.html',
    ""
  );


  $templateCache.put('views/amh-login.html',
    ""
  );


  $templateCache.put('views/amh-profile.html',
    ""
  );


  $templateCache.put('views/amh-recover-password.html',
    ""
  );


  $templateCache.put('views/amh-user-area.html',
    ""
  );


  $templateCache.put('views/amh-user.html',
    ""
  );


  $templateCache.put('views/settings/amhuser-login.html',
    " <md-list class=wb-setting-panel>  <md-subheader class=md-no-sticky>Before login</md-subheader>  <wb-ui-setting-on-off-switch title=\"Forgot password link?\" icon=vpn_key value=wbModel.content.forgot_password> </wb-ui-setting-on-off-switch>  <wb-ui-setting-on-off-switch title=\"Register button?\" icon=person_add value=wbModel.content.register> </wb-ui-setting-on-off-switch> <md-subheader class=md-no-sticky>After login</md-subheader>  <wb-ui-setting-on-off-switch title=\"Profile link?\" icon=account_circle value=wbModel.content.user_area> </wb-ui-setting-on-off-switch> <wb-ui-setting-on-off-switch title=\"Account link?\" icon=account_circle value=wbModel.content.account> </wb-ui-setting-on-off-switch>  <wb-ui-setting-on-off-switch title=\"Logout button?\" icon=settings_power value=wbModel.content.logout> </wb-ui-setting-on-off-switch> </md-list>"
  );


  $templateCache.put('views/widgets/amhuser-login.html',
    " <div layout=column> <md-progress-linear ng-disabled=\"!(ctrl.loginProcess || ctrl.logoutProcess)\" style=\"margin: 0px; padding: 0px\" md-mode=indeterminate class=md-primary md-color> </md-progress-linear>   <div style=\"text-align: center\" layout-margin ng-show=\"!ctrl.loginProcess && ctrl.loginState === 'fail'\"> <p><span md-colors=\"{color:'warn'}\" translate>{{loginMessage}}</span></p> </div>  <form ng-show=appUser.anonymous name=ctrl.myForm ng-submit=login(credit) layout=column layout-margin> <md-input-container> <label translate>Username</label> <input ng-model=credit.login name=username required> <div ng-messages=ctrl.myForm.username.$error> <div ng-message=required translate>This field is required.</div> </div> </md-input-container> <md-input-container> <label translate>Password</label> <input ng-model=credit.password type=password name=password required> <div ng-messages=ctrl.myForm.password.$error> <div ng-message=required translate>This field is required.</div> </div> </md-input-container>     <div ng-if=\"app.captcha.engine==='recaptcha'\" vc-recaptcha ng-model=credit.g_recaptcha_response theme=\"app.captcha.theme || 'light'\" type=\"app.captcha.type || 'image'\" key=app.captcha.recaptcha.key lang=\"app.captcha.language || 'fa'\"> </div> <input hide type=\"submit\"> </form> <div ng-show=appUser.anonymous layout=column layout-align=none layout-gt-xs=row layout-align-gt-xs=\"center center\" layout-margin> <a ng-show=wbModel.content.forgot_password href=users/reset-password style=\"text-decoration: none\" ui-sref=forget flex-order=1 flex-order-gt-xs=-1>{{'forgot your password?' | translate}}</a> <md-button ng-disabled=ctrl.myForm.$invalid flex-order=-1 flex-order-gt-xs=1 class=\"md-primary md-raised\" ng-click=login(credit)>{{'login' | translate}}</md-button> <md-button ng-show=wbModel.content.register ng-href=users/signup flex-order=0 flex-order-gt-xs=0 class=md-raised> {{ 'sign up' | translate }} </md-button> </div>  <div layout-margin ng-show=!appUser.anonymous layout=column layout-align=\"none center\"> <img style=\"max-width: 150px;border-radius: 50%\" src=\"{{'/api/user/'+appUser.current.id+'/avatar'}}\"> <span>{{appUser.current.first_name}} {{appUser.current.last_name}}</span> <h3>{{appUser.current.login}}</h3> </div>  <div ng-show=!appUser.anonymous layout=column layout-align=none layout-gt-xs=row layout-align-gt-xs=\"center center\" layout-margin> <md-button ng-show=wbModel.content.logout ng-click=logout() flex-order=0 flex-order-gt-xs=0 class=\"md-raised md-accent\">  {{'Logout' | translate}} </md-button> <md-button ng-show=wbModel.content.user_area ng-href=users/profile flex-order=1 flex-order-gt-xs=-1 class=md-raised>  {{'Profile' | translate}} </md-button> <md-button ng-show=wbModel.content.account ng-href=users/account flex-order=1 flex-order-gt-xs=-1 class=md-raised>  {{'Account' | translate}} </md-button> </div> </div>"
  );


  $templateCache.put('views/widgets/amhuser-page-not-found.html',
    "<div layout=row> <md-button ng-click=createContent()> <wb-icon>new</wb-icon> <span translate>create new page</span> </md-button> </div>"
  );

}]);
